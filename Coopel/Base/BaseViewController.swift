//
//  BaseViewController.swift
//  Coopel
//
//  Created by Carlos A Pelcastre Carmona on 22/03/22.
//

import UIKit

var navigationView: NavigationView!

class BaseViewController: UIViewController {

    var navigationView: NavigationView!

    override func viewDidLoad() {
        super.viewDidLoad()
        let gradient = CAGradientLayer()
        gradient.frame = view.bounds
        gradient.colors = [UIColor(named: "greenDark")?.cgColor, UIColor.black.cgColor]
        view.layer.insertSublayer(gradient, at: 0)
        createView()
        addViews()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

}

extension BaseViewController {
    func createView() {
        navigationController?.navigationBar.isHidden = true
        view.backgroundColor = .white
        navigationView = NavigationView(context: self, leftItemType: .NONE, centerItemType: .NONE, rigthItemType: .NONE)
    }

    func addViews() {
        view.addSubview(navigationView)
    }
}
