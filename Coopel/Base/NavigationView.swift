//
//  NavigationView.swift
//  Coopel
//
//  Created by Carlos A Pelcastre Carmona on 22/03/22.
//

import UIKit
 //Menu de navegacion custom

//Posible casos para el boton izquierdo
public enum NavigationItemType {
    case NONE
    case BACK
}

@objc protocol NavigationViewDelegate {
    @objc optional func leftButtonPressed()
    @objc optional func rightButtonPressed()
}


class NavigationView: UIView {
    var bottomView: UIView!
    weak var delegate: NavigationViewDelegate?
    private var leftView: UIView!
    private var centerView: UIView!
    private var titleLabel : UILabel!
    private var subTitleLabel : UILabel!
    private var rightView: UIView!


    convenience init(context: UIViewController, leftItemType: NavigationItemType, centerItemType: NavigationItemType, rigthItemType: NavigationItemType) {
        self.init(frame: CGRect(x: 0, y: 0, width: context.view.frame.width, height: UIApplication.shared.statusBarFrame.height + 78))
       

    }


    func setUpLeftView() {
        let leftButton = UIButton(type: .system)
        leftButton.setImage(UIImage(named: "flecha_izquierda"), for: .normal)
        leftButton.tintColor = .black
        leftButton.addTarget(self, action: #selector(leftButtonPressed), for: .touchUpInside)
        leftView = leftButton
        leftView.translatesAutoresizingMaskIntoConstraints = false
        leftView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(leftView)
        leftView.topAnchor.constraint(equalTo: self.topAnchor, constant: UIApplication.shared.statusBarFrame.height + 5).isActive = true
        leftView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 5).isActive = true
        leftView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -5).isActive = true
        leftView.widthAnchor.constraint(equalToConstant: 50).isActive = true
    }

    func setUpRigthView() {
        let rightButton = UIButton(type: .system)
        rightButton.setImage(UIImage(named: "log_Out"), for: .normal)
        rightButton.tintColor = .black
        rightButton.addTarget(self, action: #selector(rightButtonPressed), for: .touchUpInside)
        rightView = rightButton
        rightView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(rightView)
        rightView.topAnchor.constraint(equalTo: self.topAnchor, constant: UIApplication.shared.statusBarFrame.height + 5).isActive = true
        rightView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -5).isActive = true
        rightView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -5).isActive = true
        rightView.widthAnchor.constraint(equalToConstant: 50).isActive = true
    }

    private func setCenterView() {
        centerView = UIView()
        centerView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(centerView)
        centerView.topAnchor.constraint(equalTo: topAnchor, constant: UIApplication.shared.statusBarFrame.height).isActive = true
        centerView.leftAnchor.constraint(equalTo: leftAnchor, constant: 15).isActive = true
        centerView.rightAnchor.constraint(equalTo: rightAnchor, constant: -65).isActive = true
        centerView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10).isActive = true

        titleLabel = UILabel.getCustomLabel(text: "", size: 30)
        titleLabel.numberOfLines = 1
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        centerView.addSubview(titleLabel)
        titleLabel.topAnchor.constraint(equalTo:  centerView.centerYAnchor, constant: -3).isActive = true
        titleLabel.leftAnchor.constraint(equalTo: centerView.leftAnchor, constant: 10).isActive = true
        titleLabel.rightAnchor.constraint(equalTo: centerView.rightAnchor, constant: -5).isActive = true

        subTitleLabel = UILabel.getCustomLabel(text: "", size: 15)
        subTitleLabel.numberOfLines = 1
        subTitleLabel.translatesAutoresizingMaskIntoConstraints = false
        centerView.addSubview(subTitleLabel)
        subTitleLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 3).isActive = true
        subTitleLabel.leftAnchor.constraint(equalTo: centerView.leftAnchor, constant: 10).isActive = true
        subTitleLabel.rightAnchor.constraint(equalTo: centerView.rightAnchor, constant: -5).isActive = true
    }

    func setUpBottomView() {
        bottomView = UIView()
        bottomView.translatesAutoresizingMaskIntoConstraints = false
        bottomView.backgroundColor = .white
        bottomView.clipsToBounds = true
        bottomView.layer.cornerRadius = 20
        bottomView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        addSubview(bottomView)
        bottomView.heightAnchor.constraint(equalToConstant: 20).isActive = true
        bottomView.leftAnchor.constraint(equalTo: leftAnchor, constant: -2.5).isActive = true
        bottomView.rightAnchor.constraint(equalTo: rightAnchor, constant: 2.5).isActive = true
        bottomView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 10).isActive = true
    }

    func setTitle(title: String) {
        titleLabel = UILabel.getCustomLabel(text: "", size: 30)
        titleLabel.text = title
        centerView = titleLabel
        centerView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(centerView)
        centerView.topAnchor.constraint(equalTo: topAnchor, constant: UIApplication.shared.statusBarFrame.height).isActive = true
        centerView.leftAnchor.constraint(equalTo: leftAnchor, constant: 65).isActive = true
        centerView.rightAnchor.constraint(equalTo: rightAnchor, constant: -65).isActive = true
        centerView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10).isActive = true
    }

}

extension NavigationView {
    @objc private func leftButtonPressed() {
        delegate?.leftButtonPressed!()
    }

    @objc private func rightButtonPressed() {
        delegate?.rightButtonPressed!()
    }
}

