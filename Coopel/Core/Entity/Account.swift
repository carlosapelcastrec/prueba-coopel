//
//  Account.swift
//  Coopel
//
//  Created by carlos on 24/03/22.
//

import Foundation

struct Account : Codable{
    var avatar : Avatar
    var id : Int
    var iso_639_1 : String
    var iso_3166_1 : String
    var name : String
    var include_adult : Bool
    var username : String
}
