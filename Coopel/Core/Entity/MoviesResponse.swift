//
//  MoviesResponse.swift
//  Coopel
//
//  Created by Carlos A Pelcastre Carmona on 25/03/22.
//

import Foundation

struct MoviesResponse : Codable{
    var page : Int
    var results : [Movie]
    var total_results : Int
    var total_pages : Int
}

