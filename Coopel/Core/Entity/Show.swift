//
//  Show.swift
//  Coopel
//
//  Created by Carlos A Pelcastre Carmona on 25/03/22.
//

import Foundation

struct Show : Codable{
    var poster_path : String?
    var popularity : Double?
    var id : Int?
    var backdrop_path : String?
    var vote_average : Double?
    var overview : String?
    var first_air_date : String?
    var origin_country : [String]?
    var genre_ids: [Int]?
    var original_language : String?
    var vote_count : Int?
    var name : String?
    var original_name : String?
}
