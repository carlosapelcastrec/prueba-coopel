//
//  ShowsResponse.swift
//  Coopel
//
//  Created by Carlos A Pelcastre Carmona on 25/03/22.
//

import Foundation

struct ShowsResponse : Codable{
    var page : Int
    var results : [Show]
    var total_results : Int
    var total_pages : Int
}
