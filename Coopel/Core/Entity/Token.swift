//
//  Token.swift
//  Coopel
//
//  Created by Carlos A Pelcastre Carmona on 23/03/22.
//

import Foundation

struct Token : Codable{
    var success : Bool
    var expires_at : String
    var request_token : String
}
