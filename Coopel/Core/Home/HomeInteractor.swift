//
//  HomeInteractor.swift
//  Coopel
//
//  Created by Carlos A Pelcastre Carmona on 23/03/22.
//


import Foundation

final class HomeInteractor: HomeProvider {
    weak var output: HomeOutput?
    private var api = "e889e13d4f6798ed0929cd4d60cf8d4e"
    private var isMovie = false
    
    func callServicesGetMovies(type : categoryType){
        var url = URL(string: "")
        if type == .Popular {
            url = URL(string: "https://api.themoviedb.org/3/movie/popular?api_key=\(api)&language=en-US&page=1")
            isMovie = true
        }else if type == .top {
            url = URL(string: "https://api.themoviedb.org/3/movie/top_rated?api_key=\(api)&language=en-US&page=1")
            isMovie = true
        }else if type == .on{
            url = URL(string: "https://api.themoviedb.org/3/tv/on_the_air?api_key=\(api)&language=en-US&page=1")
            isMovie = false
        }else if type == .airing{
            url = URL(string: "https://api.themoviedb.org/3/tv/airing_today?api_key=\(api)&language=en-US&page=1")
            isMovie = false
        }
        if isMovie{
            if let url = url{
                URLSession.shared.dataTask(with: url){(data,response,err) in
                    if let error = err {
                        print(error.localizedDescription)
                    } else if let data = data, let response = response {
                        do {
                            let result = try JSONDecoder().decode(MoviesResponse.self, from: data)
                            self.output?.outputMovieHandler(movies: result.results)
                        }catch {
                            print("JSONSerialization error:", error)
                        }
                    }
                }.resume()
            }
        }else{
            if let url = url{
                URLSession.shared.dataTask(with: url){(data,response,err) in
                    if let error = err {
                        print(error.localizedDescription)
                    } else if let data = data, let response = response {
                        do {
                            let result = try JSONDecoder().decode(ShowsResponse.self, from: data)
                            self.output?.outputShowHandler(shows: result.results)
                        }catch {
                            print("JSONSerialization error:", error)
                        }
                    }
                }.resume()
            }
        }
    }
    
    func callLogOut(){
        UserDefaults.standard.set("false", forKey: "authentication")
        self.output?.outputHomeHandler()
    }
    
}
