//
//  HomeInterfaces.swift
//  Coopel
//
//  Created by Carlos A Pelcastre Carmona on 23/03/22.
//

import UIKit
// MARK: - Protocol - Funcines en las que el Presenter comparte datos con el View Controller
protocol HomeView: class {
    func getMovies(movies: [Movie])
    func getShows(shows: [Show])
}
// MARK: - Protocol - Funciones en las que el View Controler al Presenter
protocol HomeEventHandler {
    func showProfileView()
    func logOut()
    func getMovies(type : categoryType)
}
// MARK: - Protocol - Funciones que comunican al Presenter con el Interactos
protocol HomeProvider {
    func callServicesGetMovies(type : categoryType)
    func callLogOut()

}
// MARK: - Protocol - Funciones en las que el Interactor envia datos al Presenter
protocol HomeOutput: class {
    func outputHomeHandler()
    func outputMovieHandler(movies: [Movie])
    func outputShowHandler(shows: [Show])
}
