//
//  HomePresenter.swift
//  Coopel
//
//  Created by Carlos A Pelcastre Carmona on 23/03/22.
//

import Foundation

class HomePresenter {

    weak var view: HomeView?
    var provider: HomeProvider?
    var wireframe: HomeWireframe?

}

// MARK: - Extensions - Ejecuta en el presenter los llamados del View Controller
extension HomePresenter: HomeEventHandler {
    func getMovies(type: categoryType) {
        provider?.callServicesGetMovies(type: type)
    }
    
    func showProfileView() {
        wireframe?.showProfile()
    }
    
    func logOut() {
        provider?.callLogOut()
    }
    
    func showHomeView(){
        wireframe?.showSignIn()
    }
}

// MARK: - Extensions - Ejecuta en el presenter los llamados del Interactor
extension HomePresenter: HomeOutput {
    func outputMovieHandler(movies: [Movie]) {
        view?.getMovies(movies: movies)
    }
    
    func outputShowHandler(shows: [Show]) {
        view?.getShows(shows: shows)
    }
    
    func outputHomeHandler() {
        self.showHomeView()
    }
}
