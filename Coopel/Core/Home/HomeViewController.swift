//
//  HomeViewController.swift
//  Coopel
//
//  Created by Carlos A Pelcastre Carmona on 23/03/22.
//


import UIKit

final class HomeViewController: BaseViewController{
    
    private var selected = 1
    
    var headerView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIApplication.shared.statusBarFrame.height + 30))
    
    var titleLabel: UILabel = {
        var label = UILabel()
        label.text = "TV Shows"
        label.textColor = UIColor.white
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    var menuButton : UIButton = {
        let image = UIImage(systemName: "list.bullet", withConfiguration:UIImage.SymbolConfiguration(weight: .bold))?.withTintColor(.white, renderingMode: .alwaysOriginal)
        
        var button = UIButton()
        button.setImage(image, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    var popularButton : UIButton = {
        let button = UIButton()
        button.setTitle("Popular", for: .normal)
        button.setTitleColor(UIColor.white, for: .normal)
        button.titleEdgeInsets = UIEdgeInsets(top: 5,left: 5,bottom: 5,right: 5)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    var topButton : UIButton = {
        let button = UIButton()
        button.setTitle("Top Rated", for: .normal)
        button.setTitleColor(UIColor.white, for: .normal)
        button.titleEdgeInsets = UIEdgeInsets(top: 5,left: 5,bottom: 5,right: 5)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    var onButton : UIButton = {
        let button = UIButton()
        button.setTitle("On TV", for: .normal)
        button.setTitleColor(UIColor.white, for: .normal)
        button.titleEdgeInsets = UIEdgeInsets(top: 5,left: 5,bottom: 5,right: 5)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    var airingButton : UIButton = {
        let button = UIButton()
        button.setTitle("Airing Today", for: .normal)
        button.setTitleColor(UIColor.white, for: .normal)
        button.titleEdgeInsets = UIEdgeInsets(top: 5,left: 5,bottom: 5,right: 5)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    
    var movies : [Movie] = []
    var shows : [Show] = []
    
    private let collectionView: UICollectionView = {
        let viewLayout = UICollectionViewFlowLayout()
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: viewLayout)
        collectionView.backgroundColor = .clear
        return collectionView
    }()
    
    private enum LayoutConstant {
        static let spacing: CGFloat = 16.0
        static let itemHeight: CGFloat = 300.0
    }
    
    // MARK: - Public properties -
    var eventHandler: HomeEventHandler!
    
    // MARK: - Lifecycle -
    override func viewDidLoad() {
        super.viewDidLoad()
        eventHandler.getMovies(type: .Popular)
        setupHeader()
        setupBody()
        setupViews()
        setupLayouts()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    private func setupViews() {
        view.backgroundColor = .white
        view.addSubview(collectionView)
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(MovieCell.self, forCellWithReuseIdentifier: MovieCell.identifier)
    }
    
    private func setupLayouts() {
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            collectionView.topAnchor.constraint(equalTo: popularButton.bottomAnchor, constant: 24),
            collectionView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
            collectionView.leftAnchor.constraint(equalTo: view.leftAnchor),
            collectionView.rightAnchor.constraint(equalTo: view.rightAnchor)
        ])
    }
    
    init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    func setupHeader(){
        self.view.addSubview(headerView)
        headerView.backgroundColor = UIColor.gray.withAlphaComponent(0.2)
        
        headerView.addSubview(titleLabel)
        titleLabel.topAnchor.constraint(equalTo: headerView.topAnchor, constant: 40.0).isActive =  true
        titleLabel.centerXAnchor.constraint(equalTo: headerView.centerXAnchor).isActive = true
        
        headerView.addSubview(menuButton)
        menuButton.topAnchor.constraint(equalTo: headerView.topAnchor, constant: 40.0).isActive =  true
        menuButton.trailingAnchor.constraint(equalTo: headerView.trailingAnchor, constant: -8).isActive = true
        menuButton.addTarget(self, action: #selector(getActionSheet), for: .touchUpInside)
    }
    
    func setupBody(){
        view.addSubview(popularButton)
        popularButton.backgroundColor = selected == 1 ? UIColor.gray : UIColor.gray.withAlphaComponent(0.5)
        popularButton.topAnchor.constraint(equalTo: headerView.bottomAnchor, constant: 40.0).isActive = true
        popularButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 8.0) .isActive =  true
        popularButton.addTarget(self, action: #selector(getPopular), for: .touchUpInside)
        
        view.addSubview(topButton)
        topButton.backgroundColor = selected == 2 ? UIColor.gray : UIColor.gray.withAlphaComponent(0.5)
        topButton.topAnchor.constraint(equalTo: headerView.bottomAnchor, constant: 40.0).isActive = true
        topButton.leadingAnchor.constraint(equalTo: popularButton.trailingAnchor, constant: 0) .isActive =  true
        topButton.addTarget(self, action: #selector(getTop), for: .touchUpInside)
        
        view.addSubview(onButton)
        onButton.backgroundColor = selected == 3 ? UIColor.gray : UIColor.gray.withAlphaComponent(0.5)
        onButton.topAnchor.constraint(equalTo:  headerView.bottomAnchor, constant: 40.0).isActive = true
        onButton.leadingAnchor.constraint(equalTo: topButton.trailingAnchor, constant: 0) .isActive =  true
        onButton.addTarget(self, action: #selector(getOn), for: .touchUpInside)
        
        view.addSubview(airingButton)
        airingButton.backgroundColor = selected == 4 ? UIColor.gray : UIColor.gray.withAlphaComponent(0.5)
        airingButton.topAnchor.constraint(equalTo:  headerView.bottomAnchor, constant: 40.0).isActive = true
        airingButton.leadingAnchor.constraint(equalTo: onButton.trailingAnchor, constant: 0).isActive = true
        airingButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -8).isActive = true
        airingButton.addTarget(self, action: #selector(getAiring), for: .touchUpInside)
    }
    
    
    @objc func getPopular(){
        selected = 1
        eventHandler.getMovies(type: .Popular)
    }
    
    @objc func getTop(){
        selected = 2
        eventHandler.getMovies(type: .top)
    }
    
    @objc func getOn(){
        movies = []
        selected = 3
        eventHandler.getMovies(type: .on)
    }
    
    @objc func getAiring(){
        movies = []
        selected = 4
        eventHandler.getMovies(type: .airing)
    }
    
    @objc func getActionSheet(){
        let alertController = UIAlertController(title: "What do you want to do?", message: "", preferredStyle: .actionSheet)
        
        let sendButton = UIAlertAction(title: "View Profile", style: .default, handler: { (action) -> Void in
            self.eventHandler.showProfileView()
        })
        
        let  deleteButton = UIAlertAction(title: "Log Out", style: .destructive, handler: { (action) -> Void in
            self.eventHandler.logOut()
        })
        
        let cancelButton = UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) -> Void in
        })
        
        
        alertController.addAction(sendButton)
        alertController.addAction(deleteButton)
        alertController.addAction(cancelButton)
        
        self.navigationController!.present(alertController, animated: true, completion: nil)
    }
}

// MARK: - Extensions -
extension HomeViewController: HomeView {
    func getMovies(movies: [Movie]) {
        self.movies = movies
        DispatchQueue.main.async {
            self.collectionView.reloadData()
        }
    }
    
    func getShows(shows: [Show]) {
        self.shows = shows
        DispatchQueue.main.async {
            self.collectionView.reloadData()
        }
    }
    
}

extension HomeViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if movies.isEmpty {  return shows.count }else{ return movies.count }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MovieCell.identifier, for: indexPath) as! MovieCell
        if movies.isEmpty{
            let show = shows[indexPath.row]
            cell.setup(with: Movie(), show: show, isMovie: false)
        }else{
            let movie = movies[indexPath.row]
            cell.setup(with: movie, show: Show(), isMovie: true)
        }
        cell.contentView.backgroundColor = UIColor(named: "greenDark")?.withAlphaComponent(0.5)
        return cell
    }
}

extension HomeViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = itemWidth(for: view.frame.width, spacing: LayoutConstant.spacing)
        
        return CGSize(width: width, height: LayoutConstant.itemHeight)
    }
    
    func itemWidth(for width: CGFloat, spacing: CGFloat) -> CGFloat {
        let itemsInRow: CGFloat = 2
        
        let totalSpacing: CGFloat = 2 * spacing + (itemsInRow - 1) * spacing
        let finalWidth = (width - totalSpacing) / itemsInRow
        
        return floor(finalWidth)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: LayoutConstant.spacing, left: LayoutConstant.spacing, bottom: LayoutConstant.spacing, right: LayoutConstant.spacing)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return LayoutConstant.spacing
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return LayoutConstant.spacing
    }
}



protocol ReusableView: AnyObject {
    static var identifier: String { get }
}

final class MovieCell: UICollectionViewCell {
    
    private enum Constants {
        // MARK: contentView layout constants
        static let contentViewCornerRadius: CGFloat = 4.0
        
        // MARK: profileImageView layout constants
        static let imageHeight: CGFloat = 80.0
        
        // MARK: Generic layout constants
        static let verticalSpacing: CGFloat = 8.0
        static let horizontalPadding: CGFloat = 16.0
        static let verticalPadding: CGFloat = 8.0
    }
    
    private let ImageView: UIImageView = {
        let imageView = UIImageView(frame: .zero)
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    
    let title: UILabel = {
        let label = UILabel(frame: .zero)
        label.textAlignment = .center
        label.numberOfLines = 0
        label.textColor = UIColor(named: "greenLight")
        return label
    }()
    
    let release_date: UILabel = {
        let label = UILabel(frame: .zero)
        label.textAlignment = .center
        label.numberOfLines = 0
        label.textColor = UIColor(named: "greenLight")
        return label
    }()
    
    let vote_average: UILabel = {
        let label = UILabel(frame: .zero)
        label.textAlignment = .center
        label.textColor = UIColor(named: "greenLight")
        return label
    }()
    
    let overview: UILabel = {
        let label = UILabel(frame: .zero)
        label.textAlignment = .left
        label.numberOfLines = 4
        label.textColor = UIColor.white
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
        setupViews()
        setupLayouts()
    }
    
    private func setupViews() {
        contentView.clipsToBounds = true
        contentView.layer.cornerRadius = Constants.contentViewCornerRadius
        contentView.backgroundColor = .white
        
        contentView.addSubview(ImageView)
        contentView.addSubview(title)
        contentView.addSubview(release_date)
        contentView.addSubview(vote_average)
        contentView.addSubview(overview)
    }
    
    private func setupLayouts() {
        ImageView.translatesAutoresizingMaskIntoConstraints = false
        title.translatesAutoresizingMaskIntoConstraints = false
        release_date.translatesAutoresizingMaskIntoConstraints = false
        vote_average.translatesAutoresizingMaskIntoConstraints = false
        overview.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            ImageView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            ImageView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            ImageView.topAnchor.constraint(equalTo: contentView.topAnchor),
            ImageView.heightAnchor.constraint(equalToConstant: Constants.imageHeight)
        ])
        
        NSLayoutConstraint.activate([
            title.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: Constants.horizontalPadding),
            title.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -Constants.horizontalPadding),
            title.topAnchor.constraint(equalTo: ImageView.bottomAnchor, constant: Constants.verticalPadding)
        ])
        
        NSLayoutConstraint.activate([
            release_date.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: Constants.horizontalPadding),
            release_date.topAnchor.constraint(equalTo: title.bottomAnchor, constant: Constants.verticalPadding)
        ])
        
        NSLayoutConstraint.activate([
            vote_average.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -Constants.horizontalPadding),
            vote_average.topAnchor.constraint(equalTo: title.bottomAnchor, constant: Constants.verticalPadding),
        ])
        
        NSLayoutConstraint.activate([
            overview.leadingAnchor.constraint(equalTo:  contentView.leadingAnchor, constant: Constants.horizontalPadding),
            overview.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -Constants.horizontalPadding),
            overview.topAnchor.constraint(equalTo: release_date.bottomAnchor, constant: Constants.verticalPadding),
            overview.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -Constants.verticalPadding)
        ])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup(with movie: Movie,show: Show, isMovie : Bool) {
        if isMovie{
            let url = URL(string: "https://image.tmdb.org/t/p/w500/\(movie.poster_path ?? "")")!
            if let data = try? Data(contentsOf: url) {
                ImageView.image = UIImage(data: data)
            }
            title.text = movie.title ?? ""
            release_date.text = movie.release_date ?? ""
            vote_average.text = "\(movie.vote_average ?? 0.0)"
            overview.text = movie.overview ?? ""
        }else{
            let url = URL(string: "https://image.tmdb.org/t/p/w500/\(show.poster_path ?? "")")!
            if let data = try? Data(contentsOf: url) {
                ImageView.image = UIImage(data: data)
            }
            title.text = show.name ?? ""
            release_date.text = show.first_air_date ?? ""
            vote_average.text = "\(show.vote_average ?? 0.0)"
            overview.text = show.overview ?? ""
        }
    }
}


extension MovieCell: ReusableView {
    static var identifier: String {
        return String(describing: self)
    }
}
