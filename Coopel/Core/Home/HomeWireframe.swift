//
//  HomeWireframe.swift
//  Coopel
//
//  Created by Carlos A Pelcastre Carmona on 23/03/22.
//

import UIKit

class HomeWireframe {
    private var view: HomeViewController?
    private var presenter: HomePresenter?
    private var interactor: HomeInteractor?
    private var window: UIWindow?

    init(in window: UIWindow?) {
        self.view = HomeViewController()
        self.presenter = HomePresenter()
        self.interactor = HomeInteractor()
        
        self.view?.eventHandler = self.presenter
        self.interactor?.output = self.presenter
        self.presenter?.view = self.view
        self.presenter?.provider = self.interactor
        self.presenter?.wireframe = self
        self.window = window
    }

    func showHomeView() {
        self.window?.rootViewController = UINavigationController(rootViewController: self.view!)
        self.window?.makeKeyAndVisible()
    }
    
    func showProfile() {
        let ProfileView = ProfileWireframe(in: window)
        ProfileView.showProfileView()
    }
    
    func showSignIn() {
        let SignInView = SignInWireframe(in: window)
        SignInView.showSignInView()
    }
}

