//
//  SignInInteractor.swift
//  Coopel
//
//  Created by Carlos A Pelcastre Carmona on 22/03/22.
//


import Foundation

final class SignInInteractor: SignInProvider {
    weak var output: SignInOutput?
    
    private var api = "e889e13d4f6798ed0929cd4d60cf8d4e"
    
    func callServiceGetToken(_ completion: @escaping (Result<(Token, URLResponse), Error>) -> Void){
        let url = URL(string: "https://api.themoviedb.org/3/authentication/token/new?api_key=\(api)")
        if let url = url{
            URLSession.shared.dataTask(with: url){(data,response,err) in
                if let error = err {
                    completion(.failure(error))
                } else if let data = data, let response = response {
                    do {
                        let result = try JSONDecoder().decode(Token.self, from: data)
                        completion(.success((result, response)))
                    }catch {
                        print("JSONSerialization error:", error)
                    }
                }
            }.resume()
        }
    }
    
    func callServiceSignIn(username : String, password : String) {
        var tokenResponse = Token(success: false, expires_at: "", request_token: "")
        callServiceGetToken(){ result in
            do{
                tokenResponse = try result.get().0
            }catch{
                print("objeto vacio")
            }
        }
        let UrlSession = URLSession.shared
        guard let url = URL(string: "https://api.themoviedb.org/3/authentication/token/validate_with_login?api_key=\(api)") else {
            print("Error: cannot create URL")
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        ///Se construye  diccionario
        let json : Dictionary<String,Any> = [
            "username": username,
            "password": password,
            "request_token" : tokenResponse.request_token
        ]
        let jsonData = try! JSONSerialization.data(withJSONObject: json, options: []) //se transforma a json
        let task = UrlSession.uploadTask(with: request, from: jsonData){data, response, error in
            if let response = response { //se utiliza repsonse para conocer si da un 200
                let httpResponse = response as! HTTPURLResponse
                if httpResponse.statusCode == 200{ // si regresa un 200 se continua con el flujo
                    DispatchQueue.main.async {
                        print("Login")
                        UserDefaults.standard.set("true", forKey: "authentication")
                        self.output?.outputSingInHandler()
                    }
                } else {
                    //se harcodea texto porque el servicio no regresa un error
                    self.output?.outputErrorHandler(errorText: "Invalid username and/or password")
                }
            }
        }
        task.resume()
    }
}

