//
//  SignInInterfaces.swift
//  Coopel
//
//  Created by Carlos A Pelcastre Carmona on 22/03/22.
//

import UIKit
// MARK: - Protocol - Funcines en las que el Presenter comparte datos con el View Controller
protocol SignInView: class {
    func getError(errorText : String)
}
// MARK: - Protocol - Funciones en las que el View Controler al Presenter
protocol SignInEventHandler {
    func tapSignIn(username : String, password : String)
    func showHomeView()
}
// MARK: - Protocol - Funciones que comunican al Presenter con el Interactos
protocol SignInProvider {
    func callServiceSignIn(username : String, password : String)
    
}
// MARK: - Protocol - Funciones en las que el Interactor envia datos al Presenter
protocol SignInOutput: class {
    func outputSingInHandler()
    func outputErrorHandler(errorText : String)
}
