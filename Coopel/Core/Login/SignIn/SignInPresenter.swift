//
//  SignInPresenter.swift
//  Coopel
//
//  Created by Carlos A Pelcastre Carmona on 22/03/22.
//

import Foundation

class SignInPresenter {

    weak var view: SignInView?
    var provider: SignInProvider?
    var wireframe: SignInWireframe?

}

// MARK: - Extensions - Ejecuta en el presenter los llamados del View Controller
extension SignInPresenter: SignInEventHandler {
    func tapSignIn(username : String, password : String) {
        provider?.callServiceSignIn(username: username, password: password)
    }

    func showHomeView(){
        wireframe?.showHome()
    }
}

// MARK: - Extensions - Ejecuta en el presenter los llamados del Interactor
extension SignInPresenter: SignInOutput {
    func outputErrorHandler(errorText: String) {
        view?.getError(errorText: errorText)
    }
    
    func outputSingInHandler() {
        self.showHomeView()
    }
}
