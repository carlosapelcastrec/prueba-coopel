//
//  SignInViewController.swift
//  Coopel
//
//  Created by Carlos A Pelcastre Carmona on 22/03/22.
//


import UIKit

final class SignInViewController: BaseViewController {
    var logoImage : UIImageView = {
        let image = UIImageView()
        image.image = UIImage(named: "logo")
        image.contentMode = UIView.ContentMode.scaleAspectFit
        image.frame.size.width = 100
        image.frame.size.height = 100
        return image
    }()
    
    var titleLabel : UILabel!
    var emailTextField : UITextField!
    var passwordTextField  : UITextField!
    var errorLabel : UILabel!
    var SignInButton : UIButton!
    
    // MARK: - Public properties -
    var eventHandler: SignInEventHandler!
    
    // MARK: - Lifecycle -
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    func setupView(){
        view.addSubview(logoImage)
        logoImage.topAnchor.constraint(equalTo: view.topAnchor, constant: 40).isActive = true
        logoImage.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
        titleLabel =  UILabel.getCustomLabel(text: "¡Hola \n de nuevo!", size: 30)
        titleLabel.textColor = UIColor(named: "greenLight")
        view.addSubview(titleLabel)
        titleLabel.topAnchor.constraint(equalTo: logoImage.bottomAnchor, constant: 24).isActive = true
        titleLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 8).isActive = true
        titleLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -8).isActive = true
        
        emailTextField =  UITextField.getCustomTextField(text: "Correo Electronico", style: true)
        view.addSubview(emailTextField)
        emailTextField.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 16).isActive = true
        emailTextField.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 8).isActive = true
        emailTextField.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -8).isActive = true
        
        passwordTextField =  UITextField.getCustomTextField(text: "Contraseña",style: true)
        passwordTextField.isSecureTextEntry = true
        view.addSubview(passwordTextField)
        passwordTextField.topAnchor.constraint(equalTo: emailTextField.bottomAnchor, constant: 16).isActive = true
        passwordTextField.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 8).isActive = true
        passwordTextField.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -8).isActive = true
        
        errorLabel =  UILabel.getCustomLabel(text: "", size: 15)
        errorLabel.textColor = UIColor(named: "redLight")
        view.addSubview(errorLabel)
        errorLabel.topAnchor.constraint(equalTo: passwordTextField.bottomAnchor, constant: 24).isActive = true
        errorLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
        SignInButton = UIButton.getCustomButton(text: "Entrar", textColor: UIColor.white, fontSize: 14, backGroundcolor: UIColor(named: "greenLight")!, cornerRadious: 20, borderColor: .clear)
        SignInButton.addTarget(self, action: #selector(tapSignIn), for: .touchUpInside)
        view.addSubview(SignInButton)
        SignInButton.topAnchor.constraint(equalTo: errorLabel.bottomAnchor, constant: 16).isActive = true
        SignInButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 8).isActive = true
        SignInButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -8).isActive = true
    }
    
    @objc func tapSignIn(){
        eventHandler.tapSignIn(username: emailTextField.text ?? "", password: passwordTextField.text ?? "")
    }
}

// MARK: - Extensions -
extension SignInViewController: SignInView {
    func getError(errorText: String) {
        DispatchQueue.main.async {
            self.errorLabel.text = errorText
        }
       
        
    }
}



