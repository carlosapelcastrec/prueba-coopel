//
//  SignInWireframe.swift
//  Coopel
//
//  Created by Carlos A Pelcastre Carmona on 22/03/22.
//

import UIKit

class SignInWireframe {
    private var view: SignInViewController?
    private var presenter: SignInPresenter?
    private var interactor: SignInInteractor?
    private var window: UIWindow?
    private var navigation: UINavigationController?

    init(in window: UIWindow?) {
        self.view = SignInViewController()
        self.presenter = SignInPresenter()
        self.interactor = SignInInteractor()
        
        self.view?.eventHandler = self.presenter
        self.interactor?.output = self.presenter
        self.presenter?.view = self.view
        self.presenter?.provider = self.interactor
        self.presenter?.wireframe = self
        self.window = window
        self.navigation = self.window!.rootViewController as? UINavigationController
    }
    private func clearViper() {
        self.view?.eventHandler = nil
        self.interactor?.output = nil
        self.presenter?.view = nil
        self.presenter?.provider = nil
        self.presenter?.wireframe = nil
    }

    func showSignInView() {
        self.window?.rootViewController = UINavigationController(rootViewController: self.view!)
        self.window?.makeKeyAndVisible()
    }

    func showHome() {
        let HomeView = HomeWireframe(in: window)
        HomeView.showHomeView()
    }
}


