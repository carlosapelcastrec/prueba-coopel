//
//  ProfileInteractor.swift
//  Coopel
//
//  Created by Carlos A Pelcastre Carmona on 24/03/22.
//


import Foundation

final class ProfileInteractor: ProfileProvider {
    weak var output: ProfileOutput?
    private var api = "e889e13d4f6798ed0929cd4d60cf8d4e"
    
    func getInformationProfile(){
        let url = URL(string: " https://api.themoviedb.org/3/account?api_key=\(api)")
        if let url = url{
            URLSession.shared.dataTask(with: url){(data,response,err) in
                if let error = err {
                    print(error)
                } else if let data = data, let response = response {
                    do {
                        let result = try JSONDecoder().decode(Account.self, from: data)
                        print(result)
                    }catch {
                        print("JSONSerialization error:", error)
                    }
                }
            }.resume()
        }
    }
}
