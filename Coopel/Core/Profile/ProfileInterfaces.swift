//
//  ProfileInterfaces.swift
//  Coopel
//
//  Created by Carlos A Pelcastre Carmona on 24/03/22.
//

import UIKit
// MARK: - Protocol - Funcines en las que el Presenter comparte datos con el View Controller
protocol ProfileView: class {
    
}
// MARK: - Protocol - Funciones en las que el View Controler al Presenter
protocol ProfileEventHandler {
    func getProfile()
    func getMovieFavorites()
}
// MARK: - Protocol - Funciones que comunican al Presenter con el Interactos
protocol ProfileProvider {
    
}
// MARK: - Protocol - Funciones en las que el Interactor envia datos al Presenter
protocol ProfileOutput: class {
    
}
