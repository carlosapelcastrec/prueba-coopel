//
//  ProfilePresenter.swift
//  Coopel
//
//  Created by Carlos A Pelcastre Carmona on 24/03/22.
//

import Foundation

class ProfilePresenter {

    weak var view: ProfileView?
    var provider: ProfileProvider?
    var wireframe: ProfileWireframe?

}

// MARK: - Extensions - Ejecuta en el presenter los llamados del View Controller
extension ProfilePresenter: ProfileEventHandler {
    func getProfile() {
        
    }
    
    func getMovieFavorites() {
        
    }
    
    
}

// MARK: - Extensions - Ejecuta en el presenter los llamados del Interactor
extension ProfilePresenter: ProfileOutput {
    
}
