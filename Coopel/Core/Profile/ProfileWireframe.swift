//
//  ProfileWireframe.swift
//  Coopel
//
//  Created by Carlos A Pelcastre Carmona on 24/03/22.
//

import UIKit

class ProfileWireframe {
    private var view: ProfileViewController?
    private var presenter: ProfilePresenter?
    private var interactor: ProfileInteractor?
    private var window: UIWindow?

    init(in window: UIWindow?) {
        self.view = ProfileViewController()
        self.presenter = ProfilePresenter()
        self.interactor = ProfileInteractor()
        
        self.view?.eventHandler = self.presenter
        self.interactor?.output = self.presenter
        self.presenter?.view = self.view
        self.presenter?.provider = self.interactor
        self.presenter?.wireframe = self
        self.window = window
    }

    func showProfileView() {
        self.window?.rootViewController = UINavigationController(rootViewController: self.view!)
        self.window?.makeKeyAndVisible()
    }

    func showHome() {
        let HomeView = HomeWireframe(in: window)
        HomeView.showHomeView()
    }

}

