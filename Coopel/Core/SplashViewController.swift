//
//  SplashViewController.swift
//  Coopel
//
//  Created by Carlos A Pelcastre Carmona on 22/03/22.
//

import Foundation
import Lottie


class SplashViewController: UIViewController {
    
    private var animationView: AnimationView?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        let gradient = CAGradientLayer()
        gradient.frame = view.bounds
        gradient.colors = [UIColor(named: "greenDark")?.cgColor, UIColor.black.cgColor]

        view.layer.insertSublayer(gradient, at: 0)
        
        animationView = .init(name: "splash")
        animationView!.frame = view.bounds
        animationView!.contentMode = .scaleAspectFit
        animationView!.loopMode = .loop
        animationView!.animationSpeed = 0.5
        view.addSubview(animationView!)
        animationView!.play()
        
    }
    
    
    
}


