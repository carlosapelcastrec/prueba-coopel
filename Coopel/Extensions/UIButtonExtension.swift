//
//  UIButtonExtension.swift
//  Coopel
//
//  Created by Carlos A Pelcastre Carmona on 22/03/22.
//

import UIKit

extension UIButton{
    public static func getCustomButton(text: String = "", textColor: UIColor, fontSize: CGFloat, backGroundcolor: UIColor, cornerRadious: CGFloat = 0, borderColor: UIColor? = nil, normalImage: UIImage? = nil, activeImage: UIImage? = nil, background: UIColor? = nil) -> UIButton {
        let button = UIButton(type: .system)
        button.setTitle(text, for: .normal)
        button.setTitleColor(textColor, for: .normal)
        button.titleLabel?.font = UIFont(name: "Helvetica Neue", size: fontSize)
        button.backgroundColor = backGroundcolor
        button.layer.cornerRadius = cornerRadious
        if borderColor != nil {
            button.layer.borderColor = borderColor?.cgColor
            button.layer.borderWidth = 2
        }
        if UIDevice.current.userInterfaceIdiom == .pad {
            button.heightAnchor.constraint(equalToConstant: 60.0).isActive = true
        }else{
            button.heightAnchor.constraint(equalToConstant: 40.0).isActive = true
        }

        button.isUserInteractionEnabled = true
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }
}
