//
//  UILabelExtension.swift
//  Coopel
//
//  Created by Carlos A Pelcastre Carmona on 22/03/22.
//

import UIKit

extension  UILabel{
    public static func getCustomLabel(text:String,size : CGFloat) -> UILabel{
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = text
        label.textColor = UIColor.black
        label.numberOfLines = 0
        if UIDevice.current.userInterfaceIdiom == .pad {
            label.font = label.font.withSize(size*2)
        }else{
            label.font = label.font.withSize(size)
        }
        return label
    }
}
