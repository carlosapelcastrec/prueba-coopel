//
//  UITextFieldExtensions.swift
//  Coopel
//
//  Created by Carlos A Pelcastre Carmona on 22/03/22.
//

import UIKit

extension UITextField{
    public static func getCustomTextField(text: String, style: Bool) -> UITextField{
        let textField = UITextField()
        textField.placeholder = text
        textField.backgroundColor = .white
        textField.font = UIFont(name: "", size: CGFloat(14.0))
        textField.textAlignment = .center
        textField.setBottomBorder(style: style)
        textField.translatesAutoresizingMaskIntoConstraints = false
        if UIDevice.current.userInterfaceIdiom == .pad {
            textField.heightAnchor.constraint(equalToConstant: 25.0).isActive = true
        }else{
            textField.heightAnchor.constraint(equalToConstant: 45.0).isActive = true
        }
        return textField
    }

    public func setBottomBorder(style : Bool) {
        self.borderStyle = .none
        if style == true{
            self.layer.backgroundColor = UIColor.white.cgColor
            self.layer.borderColor = UIColor.black.cgColor
        }else{
            self.layer.backgroundColor = UIColor.clear.cgColor
            self.layer.borderColor = UIColor.white.cgColor
        }
        self.layer.borderWidth = 3.0
        self.layer.cornerRadius = 20.0
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        self.layer.shadowOpacity = 1.0
        self.layer.shadowRadius = 0.0
    }
}
